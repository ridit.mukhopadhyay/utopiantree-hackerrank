package UtopianTreeWithRecursion;
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the growth cycle of the utopian tree");
		int n = in.nextInt();
		System.out.println(calculateHeight(n));
	}
	public static double calculateHeight(int n) {
		int h0 = 1;
		if(n == 0) {
			return h0;
		}
		else if(n % 2 == 0){
			return calculateHeight(n-1) + 1;
		}
		else {
			return 2 * calculateHeight(n-1);	
		}				
	}
}
